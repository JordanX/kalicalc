# kaliCalc

## Name
kaliCalc

## Description
kaliCalc - built using Xojo

I love the MacOS App Soulver 3 by Acqualia Software (https://soulver.app). It’s a great app, well designed and works really well. ￼


I wanted to try and build something similar as a side project. (Images are https://gitlab.com/JordanX/kalicalc/-/blob/main/kaliCalc.pdf)

kaliCalc is a MenuBar StatusItem, you launch it from the Mac Menubar.

The Main Window (and Documentation)
- in the Main Window, You can type a sentence that includes a mathematical expression, and kaliCalc will calculate the total on the right. 
- Titles are designated by a space, ‘ or - in the first character of the sentence.
- The mathematical expression must make sense (e.g. rules of precedences, operands and operators).
- To enter a subtotal, type subtotal onto a blank line, and it'll subtotal any amounts above it. 
￼
This App isn’t great ;) There’s nothing novel about it. It’s doesn’t have an amazing design, and you’re probably not going to learn a lot. The hope is that others will contribute and make it better. There are probably lots of things I could have done better, but this was a weekend project ;) 


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Download, Build, Launch App

## Usage
Compile and launch app. The main window is accessed from the Mac OS Menubar (* Calc)

## Support
TBD (Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.TB)

## Roadmap
Things that could make this app better
 Separate Pages
 Additional mathematical expresseions ^2 for example
 Date Calculations


## Contributing
Please volunteer and contribute. Submit pull requests.

## Authors and acknowledgment
Contributions

REALEasyPrefs
Originally by Chris Cormeau.
This version has been modified and updated for the new REAL Studio IDE and Frameworks by Tim Jones.
I will try to keep it updated and available on my .me homesite.
tolistim@me.com

macoslib
https://github.com/macoslib/macoslib
This is the MacOSLib project for Xojo (formerly Real Studio).
It provides additional OS X specific functionality that Xojo's own framework doesn't offer.
It even has some cross-platform functionality in order to access Mac specific data on Windows and Linux (e.g. plist files).

Evaluator XojoScript
I believe I grabbed this from the xojo blog or xojo forum. Excellent work. Thank you!
I believe Paul Lefebvre (maybe?)

PaintCellBackground
Paul Lefebvre
Thomas Robisson
Mark Sweeney
Simple, Easy, Thank You!

## License
Copyright 2023 Kaliware LLC jordan@kaliware.com 
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

